Electric Pão de Queijo

Prepare your own brazilian breakfast with a shocking pão de queijo mineiro.
All you need is one goat electric milk and three bird eggs.
Electric Pão de Queijo is a deliscious veggie breakfast.

Recipe

1 Electric Milk and 3 Eggs

Food status

25 of hunger regeneration, because eating Pão de Queijo satisfies medianly your hunger.
50 of sanity regeneration, because nobody becomes sad after eating a deliscious Pão de Queijo.
1 of health regeneration, because... it ain't healthy at all xD

---------------------------------------------------------------------------
PT-br
---------------------------------------------------------------------------

Pão de Queijo Elétrico

Prepare seu próprio café da manhã com um pão de queijo mineiro chocante.
Tudo que você precisa é um leite elétrico de cabra e três ovos de pássaro.
Pão de Queijo Elétrico é um delicioso café da manhã vegetariano.

Receita

1 Electric Milk e 3 Eggs

Status de regeneração

25 de fome, porque mata a sua fome medianamente.
50 de sanidade, porque ninguém fica triste comendo Pão de Queijo.
1 de vida, porque... cá pra nós... não é muuuito saudável. xD


---------------------------------------------------------------------------
STEAM Link
---------------------------------------------------------------------------

https://steamcommunity.com/sharedfiles/filedetails/?id=2418896089
