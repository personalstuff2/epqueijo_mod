local epqparams = require "epqmod_params"

GLOBAL.STRINGS.NAMES.EPQUEIJO = "Electric Pão de Queijo"
GLOBAL.STRINGS.CHARACTERS.GENERIC.DESCRIBE.EPQUEIJO = "YUMMY! This seems shocking tasty! Did you get it? Shocking? LOL?"

-- CREATE PREFAB
PrefabFiles = {
    epqparams.NAME,
    epqparams.SPARKS_NAME
}

AddCookerRecipe("cookpot", epqparams.RECIPE)
AddCookerRecipe("portablecookpot", epqparams.RECIPE)
AddCookerRecipe("archive_cookpot", epqparams.RECIPE)
