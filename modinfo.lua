name = "Electric Pão de Queijo"

description = [[
Prepare your own brazilian breakfast with a shocking pão de queijo mineiro. 
All you need is one goat electric milk and three bird eggs.
Electric Pão de Queijo is a deliscious veggie breakfast.
]]

author = "Vinicius F.M."
version = "0.0.1"
icon_atlas = "modicon.xml"
icon = "modicon.tex"
server_filter_tags = {"brazilian food", "pao de queijo"}

api_version = 10
all_clients_require_mod = true
dst_compatible = true
priority = 0.2
