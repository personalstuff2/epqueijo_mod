EPQUEIJO_MOD_PARAMS = {}

--if rename this mod you must rename all the files that comes with "epqueijo"
EPQUEIJO_MOD_PARAMS.NAME = "epqueijo"
EPQUEIJO_MOD_PARAMS.SPARKS_NAME = "epqsparks"
EPQUEIJO_MOD_PARAMS.MAINANIM = "idle"
EPQUEIJO_MOD_PARAMS.SPARKSANIM = "emitting"
EPQUEIJO_MOD_PARAMS.RECIPE = 
{
    name = EPQUEIJO_MOD_PARAMS.NAME,
    test = function(cooker, names, tags) return names.bird_egg == 3 and names.goatmilk end,
    priority = 1,
    weight = 1,
    foodtype = "VEGGIE",
    health = TUNING.HEALING_TINY,
    hunger = TUNING.CALORIES_MED,
    sanity = TUNING.SANITY_HUGE,
    perishtime = TUNING.PERISH_SUPERFAST,
    cooktime = 0.75
}
EPQUEIJO_MOD_PARAMS.ASSETS =
{
    Asset("ANIM", "anim/"..EPQUEIJO_MOD_PARAMS.NAME..".zip"),
    Asset("ATLAS", "images/inventoryimages/"..EPQUEIJO_MOD_PARAMS.NAME..".xml")
}
EPQUEIJO_MOD_PARAMS.FOODSTATE = "PREPARED"
EPQUEIJO_MOD_PARAMS.STACKSIZE = TUNING.STACK_SIZE_SMALLITEM
EPQUEIJO_MOD_PARAMS.TRADEVALUE = TUNING.GOLD_VALUES.MEAT
EPQUEIJO_MOD_PARAMS.INVENTORYATLAS = "images/inventoryimages/"..EPQUEIJO_MOD_PARAMS.NAME..".xml"

EPQUEIJO_MOD_PARAMS.SPARKS_ASSETS = {
    Asset("ANIM", "anim/"..EPQUEIJO_MOD_PARAMS.SPARKS_NAME..".zip"),
    Asset("SOUNDPACKAGE", "sound/"..EPQUEIJO_MOD_PARAMS.NAME..".fev"),
    Asset("SOUND", "sound/"..EPQUEIJO_MOD_PARAMS.NAME..".fsb")
}

EPQUEIJO_MOD_PARAMS.SPARKS_FMOD = "epqueijo/emitter/sparks"

return EPQUEIJO_MOD_PARAMS
