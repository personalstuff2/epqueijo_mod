require "prefabutil"

local epqparams = require("epqmod_params")

local assets = epqparams.ASSETS

local prefabs = 
{
    "spoiled_food",
    "epqsparks"
}

local function OnDropped(inst)
    if inst.epqsparks == nil then
        inst.epqsparks = inst:SpawnChild("epqsparks")
    end
end

local function OnPickup(inst)
    if inst.epqsparks ~= nil then
        inst.epqsparks:Remove()
        inst.epqsparks = nil
    end
end

local function fn(Sim)
    local inst = CreateEntity()
    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()

    MakeInventoryPhysics(inst)

    inst:AddTag("preparedfood")
    
    inst.AnimState:SetBank(epqparams.NAME)
    inst.AnimState:SetBuild(epqparams.NAME)
    inst.AnimState:PlayAnimation(epqparams.MAINANIM)

    inst.entity:SetPristine()
    
    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("edible")
    inst.components.edible.foodtype = epqparams.RECIPE.foodtype
    inst.components.edible.foodstate = epqparams.FOODSTATE

    inst.components.edible.healthvalue = epqparams.RECIPE.health
    inst.components.edible.hungervalue = epqparams.RECIPE.hunger
    inst.components.edible.sanityvalue = epqparams.RECIPE.sanity

    inst:AddComponent("stackable")
    inst.components.stackable.maxsize = epqparams.STACKSIZE


    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.atlasname = epqparams.INVENTORYATLAS
    inst.components.inventoryitem:SetOnDroppedFn(OnDropped)
    inst.components.inventoryitem:SetOnPickupFn(OnPickup)
    inst.components.inventoryitem:SetOnPutInInventoryFn(OnPickup)

    inst:AddComponent("perishable")
    inst.components.perishable:SetPerishTime(epqparams.RECIPE.perishtime)
    inst.components.perishable:StartPerishing()
    inst.components.perishable.onperishreplacement = "spoiled_food"

    inst:AddComponent("inspectable")

    inst:AddComponent("tradable")
    inst.components.tradable.goldvalue = epqparams.TRADEVALUE

    inst.epqsparks = inst:SpawnChild("epqsparks")

    return inst
end

return Prefab( "common/inventory/"..epqparams.NAME, fn, epqparams.ASSETS, prefabs )
