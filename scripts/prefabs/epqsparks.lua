require "prefabutil"

local epqparams = require("epqmod_params")

local assets = epqparams.SPARKS_ASSETS

local function OnWake(inst)
    inst.SoundEmitter:PlaySound(epqparams.SPARKS_FMOD, epqparams.SPARKS_NAME)
end

local function OnSleep(inst)
    inst.SoundEmitter:KillSound(epqparams.SPARKS_NAME)
end

local function oninit(inst)
    inst.components.playerprox:ForceUpdate()
    if not inst:IsAsleep() then
        OnWake(inst)
    end
    inst.OnEntityWake = OnWake
    inst.OnEntitySleep = OnSleep
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddSoundEmitter()
    inst.entity:AddNetwork()

    inst.AnimState:SetBank(epqparams.SPARKS_NAME)
    inst.AnimState:SetBuild(epqparams.SPARKS_NAME)

    inst.AnimState:PlayAnimation("emitting", true)

    inst:AddTag("NOCLICK")
    inst:AddTag("FX")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("playerprox")
    inst.components.playerprox:SetDist(2,3)
    inst:DoTaskInTime(0, oninit)

    return inst
end

return Prefab("common/"..epqparams.SPARKS_NAME, fn, assets)
